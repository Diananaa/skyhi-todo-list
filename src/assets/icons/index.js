import ICPlus from "./tabler_plus.svg";
import ImgEmptyStateActivity from "./activity-empty-state.svg";
import ICDetele from "./activity-item-delete-button.svg";
import ICBackArrow from "./todo-back-button.svg";
import ICArrowsSort from "./tabler_arrows-sort.svg";
import ICEdit from "./edit.svg";
import ImgEmptyStateTodo from "./todo-empty-state.png";
import ICCheck from "./tabler_check.svg";
import ICCheckDropdown from "./ceklis.svg";
import ICUpdate from "./terbaru.svg";
import ICNotFinish from "./not_finished_yet.svg";
import ICLongest from "./longest.svg";
import ICAscending from "./ascending.svg";
import ICDescending from "./descending.svg";
import ICChevronDown from "./tabler_chevron-down.svg";
import ICDeleteModal from "./modal-add-close-button.svg";

export {
  ICCheck,
  ICPlus,
  ImgEmptyStateActivity,
  ICDetele,
  ICBackArrow,
  ICArrowsSort,
  ICEdit,
  ImgEmptyStateTodo,
  ICCheckDropdown,
  ICUpdate,
  ICNotFinish,
  ICLongest,
  ICAscending,
  ICDescending,
  ICChevronDown,
  ICDeleteModal,
};
