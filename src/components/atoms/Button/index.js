import { ICPlus } from "../../../assets";

const Button = ({ sencodary, onClick, title }) => {
  return (
    <button
      onClick={onClick}
      type="button"
      className="bg-[#16ABF8] h-[54px] w-[159px] rounded-[45px] text-white font-semibold text-lg flex items-center justify-center "
    >
      {sencodary && <img alt="#" src={ICPlus} className="w-6 mr-[6px]" />}
      <p>{title}</p>
    </button>
  );
};

export default Button;
