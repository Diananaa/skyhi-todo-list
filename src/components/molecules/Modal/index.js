import { useState } from "react";
import { Button } from "../..";
import { ICDeleteModal } from "../../../assets";

const Modal = () => {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <Button sencodary onClick={() => setShowModal(true)} title="Tambah" />
      {/* <button
        className="bg-pink-500 text-white active:bg-pink-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
        type="button"
        onClick={() => setShowModal(true)}
      >
        Open regular modal
      </button> */}
      {showModal ? (
        <>
          <div className="justify-center items-center   flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto min-w-[830px]">
              {/*content*/}
              <form>
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                  {/*header*/}
                  <div className="flex items-start justify-between pl-[30px] pt-[24px] pb-[19px] border-b border-solid border-slate-200 rounded-t">
                    <p className="font-semibold text-lg">Tambah List Item</p>
                    <button
                      className="mr-[41px]"
                      type="button"
                      onClick={() => setShowModal(false)}
                    >
                      <img src={ICDeleteModal} alt="#" className="w-[24px]" />
                    </button>
                  </div>
                  {/*body*/}
                  <div className="relative ml-[30px] mr-[41px] flex flex-col">
                    <label>
                      NAMA LIST ITEM
                      <input
                        type="text"
                        name="name"
                        className="w-full border-2"
                      />
                    </label>
                  </div>

                  <div className="relative ml-[30px] w-[205px] ">
                    <label className="flex flex-col">
                      PRIORITY
                      <select
                      // value={this.state.value}
                      // onChange={this.handleChange}
                      >
                        <option value="grapefruit">Grapefruit</option>
                        <option value="lime">Lime</option>
                        <option value="coconut">Coconut</option>
                        <option value="mango">Mango</option>
                      </select>
                    </label>
                  </div>
                  {/*footer*/}
                  <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                    <Button
                      onClick={() => setShowModal(false)}
                      title="Simpan"
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default Modal;
