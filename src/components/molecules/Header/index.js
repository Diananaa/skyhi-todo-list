import React from "react";

export default function Header() {
  return (
    <header
      data-cy="header"
      className="h-[105px] bg-[#16ABF8] grid content-center "
    >
      <div data-cy="header-background" className=" flex sm:pl-[220px] pl-2 ">
        <p data-cy="header-title" className="text-2xl  text-white font-bold ">
          To Do List App
        </p>
      </div>
    </header>
  );
}
