import { useState } from "react";
import {
  ICArrowsSort,
  ICAscending,
  ICCheckDropdown,
  ICDescending,
  ICLongest,
  ICNotFinish,
  ICUpdate,
} from "../../../assets";

const Dropdown = () => {
  const list =
    "flex items-center justify-between h-[52px] py-[17px] hover:bg-gray-100 ";
  const [showOptions, SetShowOptions] = useState(false);
  const handleClick = () => {
    SetShowOptions(!showOptions);
  };
  return (
    <div class="relative inline-block text-left">
      <div>
        <button
          onClick={handleClick}
          type="button"
          className="py-[13px] px-[14px] border-2  border-[#E5E5E5] shadow-none rounded-full mr-[18px]"
          id="menu-button"
          aria-expanded="true"
          aria-haspopup="true"
        >
          <div>
            <img src={ICArrowsSort} alt="#" className="w-6" />
          </div>
        </button>
      </div>
      {showOptions && (
        <div
          className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
          role="menu"
          aria-orientation="vertical"
          aria-labelledby="menu-button"
          tabindex="-1"
        >
          <div
            className="py-1 text-[#4A4A4A] font-normal text-base  w-[235px] px-[21px] divide-y border-[#E5E5E5]"
            role="none"
          >
            <a
              href="/"
              className={list}
              role="menuitem"
              tabindex="-1"
              id="menu-item-0"
            >
              <div className="flex ">
                <img src={ICUpdate} alt="#" className="w-[18px]" />
                <p className="ml-[15px]">Terbaru</p>
              </div>
              <img src={ICCheckDropdown} alt="#" className="w-[18px]" />
            </a>

            <a
              href="/"
              className={list}
              role="menuitem"
              tabindex="-1"
              id="menu-item-0"
            >
              <div className="flex ">
                <img src={ICLongest} alt="#" className="w-[18px]" />
                <p className="ml-[15px]">Terlama</p>
              </div>
              <img src={ICCheckDropdown} alt="#" className="w-[18px]" />
            </a>
            <a
              href="/"
              className={list}
              role="menuitem"
              tabindex="-1"
              id="menu-item-0"
            >
              <div className="flex ">
                <img src={ICAscending} alt="#" className="w-[18px]" />
                <p className="ml-[15px]">A-Z</p>
              </div>
              <img src={ICCheckDropdown} alt="#" className="w-[18px]" />
            </a>
            <a
              href="/"
              className={list}
              role="menuitem"
              tabindex="-1"
              id="menu-item-0"
            >
              <div className="flex ">
                <img src={ICDescending} alt="#" className="w-[18px]" />
                <p className="ml-[15px]">Z-A</p>
              </div>
              <img src={ICCheckDropdown} alt="#" className="w-[18px]" />
            </a>
            <a
              href="/"
              className={list}
              role="menuitem"
              tabindex="-1"
              id="menu-item-0"
            >
              <div className="flex ">
                <img src={ICNotFinish} alt="#" className="w-[18px]" />
                <p className="ml-[15px]">Belum Selesai</p>
              </div>
              <img src={ICCheckDropdown} alt="#" className="w-[18px]" />
            </a>
          </div>
        </div>
      )}
    </div>
  );
};
export default Dropdown;
