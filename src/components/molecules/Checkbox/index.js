import { ICCheck, ICDetele, ICEdit } from "../../../assets";

const Checkbox = () => {
  return (
    <div className="flex justify-between px-[28px] rounded-xl mb-[10px] items-center h-[80px] bg-white drop-shadow-[0px_6px_10px_rgba(0,0,0,0.1)]">
      <div className="flex ">
        {/* <div className="w-[25px] h-[25px] bg-white border-2 border-[#C7C7C7]" /> */}
        <div className="w-[25px] h-[25px] flex justify-center items-center bg-[#16ABF8]">
          <img src={ICCheck} alt="#" className="w-[14px]" />
        </div>
        <div className=" flex items-center pl-[22px] pr-4">
          <div className="w-[9px] h-[9px] bg-[#ED4C5C] rounded-full" />
        </div>
        {/* <p className="font-medium text-lg pr-[19.33px]">Telur ayam</p> */}
        <p className="font-medium text-[#888888] line-through text-lg pr-[19.33px]">
          Telur ayam
        </p>

        <img src={ICEdit} alt="#" className="w-[12.77px] " />
      </div>
      <img src={ICDetele} alt="#" className="[w-16px]" />
    </div>
  );
};

export default Checkbox;
