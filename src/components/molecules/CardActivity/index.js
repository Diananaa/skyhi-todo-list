import { ICDetele } from "../../../assets";

const CardActivity = () => {
  return (
    <div
      data-cy="activity-item"
      className="w-full h-[234px] flex flex-col justify-between rounded-xl bg-white drop-shadow-[0px_6px_10px_rgba(0,0,0,0.1)]"
    >
      <p className="px-[27px] py-[22px] font-bold text-[18px]">
        Daftar Belanja Bulanan
      </p>
      <div className="flex justify-between px-[27px] mb-[25px]">
        <p className="text-[#888888] font-medium text-sm">5 Oktober 2021</p>
        <img src={ICDetele} alt="#" className="w-6 h-6 " />
      </div>
    </div>
  );
};

export default CardActivity;
