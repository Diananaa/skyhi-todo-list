import { Dropdown, Modal } from "..";
import { ICBackArrow, ICEdit } from "../../../assets/icons";

const NavigationDetail = () => {
  return (
    <nav className="z-0 flex justify-between mt-[43px] mb-[54px]">
      <div className="flex items-center">
        <img src={ICBackArrow} alt="#" className="w-8 h-8 mr-[19px]" />
        <p className="font-bold text-[36px]">New Activity</p>
        <img src={ICEdit} alt="#" className="w-[15.33px] ml-[23px]" />
      </div>
      <div className="flex ">
        {/* <div className="py-[13px] px-[14px] border-2 border-[#E5E5E5] rounded-full mr-[18px]">
          <img src={ICArrowsSort} alt="#" className="w-6" />
        </div> */}
        {/* <div className="absolute right-[450px] top-[190px] z-10  "> */}
        <div className="absolut z-10  ">
          <Dropdown />
        </div>
        <Modal />
      </div>
    </nav>
  );
};

export default NavigationDetail;
