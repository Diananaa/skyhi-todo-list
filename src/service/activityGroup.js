import axios from "axios";

const list = () => {
  return axios.get(
    "https://todo.api.devcode.gethired.id/activity-groups?email=d1anasite04@gmail.com"
  );
};

const createActivity = (data) => {
  return axios.post(
    "https://todo.api.devcode.gethired.id/activity-groups",
    data
  );
};

export { list, createActivity };
