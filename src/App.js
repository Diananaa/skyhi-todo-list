import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Header } from "./components";
import { Dashboard, ItemList } from "./pages";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <main>
        <Routes>
          <Route path="/" element={<Dashboard />} />
          <Route path="/item-list" element={<ItemList />} />
        </Routes>
      </main>
    </BrowserRouter>
  );
}

export default App;
