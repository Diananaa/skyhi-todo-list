import { useEffect, useState } from "react";
import { ImgEmptyStateActivity } from "../../assets";
import { Button, CardActivity } from "../../components";
import * as activityAPI from "../../service/activityGroup";

export default function Dashboard() {
  const [activity, setActivity] = useState([]);

  useEffect(() => {
    activityAPI.list().then((res) => {
      console.log(res.data.data);
      setActivity(res.data.data);
    });
  }, []);
  return (
    <>
      <body className="sm:px-[220px] pl-2 ">
        {/* <Navigation /> */}
        <p data-cy="activity-title" className="font-bold text-[36px]">
          Activity
        </p>
        <Button datacy="activity-add-button" sencodary title={"Tambah"} />
        <section className="container grid grid-cols-2 lg:grid-cols-4 gap-5">
          {activity.length < 1 ? (
            <img
              data-cy="activity-empty-state"
              src={ImgEmptyStateActivity}
              alt="#"
            />
          ) : (
            activity.map((item) => <CardActivity />)
          )}
        </section>
      </body>
    </>
  );
}
